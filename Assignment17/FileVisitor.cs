﻿namespace Assignment17;

public class FileVisitor
{
	public event EventHandler FileFound;

	public void ScanFolder(string path)
	{
		if (!Directory.Exists(path))
			return;

		var files = Directory.GetFiles(path);
		foreach (var f in files)
		{
			var file = new FileArgs
			{
				Name = Path.GetFileName(f),
			};
			FileFound?.Invoke(this, file);

		}

		var dir = Directory.GetDirectories(path);
		foreach (var d in dir)
		{
			ScanFolder(d);
		}

	}

}

public class FileArgs : EventArgs
{
	public string Name { get; set; }
}
