﻿namespace Assignment17
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var test = new List<Test> { new() { A = 1 }, new() { A = 2 }, new() { A = 3 } };
			Console.WriteLine(test.GetMax(i => i.A).A);

			var finder = new FileVisitor();
			finder.FileFound += (sender, eventArgs) =>
			{
				Console.WriteLine(((FileArgs)eventArgs).Name);
			};
			finder.ScanFolder("C:/ps");
			
			Console.ReadLine();
		}
	}

	internal class Test
	{
		public float A;
	}
}