﻿namespace Assignment17;

public static class Extensions
{
	public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
	{
		if (!e.Any())
			return null;

		T max = null;
		foreach (var item in e)
		{
			max = max == null || getParameter(item) > getParameter(max)
				? item
				: max;
		}

		return max;
	}
}